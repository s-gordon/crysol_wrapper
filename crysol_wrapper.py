#!/usr/bin/env python
# AUTHOR:   Shane Gordon
# FILE:     crysol_wrapper.py
# ROLE:     Python wrapper for CRYSOL
# CREATED:  2015-05-16 22:27:15
# MODIFIED: 2015-05-18 13:11:56

import argparse
import pexpect
import sys, os
import logging
import glob

logging.basicConfig(format='%(levelname)s:\t%(message)s', level=logging.DEBUG)

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

def filecheck(file):
    try:
        f = open('%s' % file, 'r')
        f.close()
    except IOError:
        logging.error('There was an error opening the file: %s.' % file)
        logging.error('Have you spelt the file name correctly?')
        logging.error('Exiting')
        sys.exit(1)

def assure_path_exists(path):
    if not os.path.exists(path):
        os.makedirs(path)

parser=MyParser(description='Python wrapper for CRYSOL',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-c', action="store", dest="crysol", 
        default="/usr/local/bin/crysol", help="CRYSOL executable directory")
parser.add_argument('-p', action="store", dest="pdbfile", 
        required = True, help="PDB file for generating theoretical scattering curve")
parser.add_argument('-H', action="store", dest="order_harmonics", default="30",
        type=int, help="Order of Harmonics")
parser.add_argument('-F', action="store", dest="order_Fibonacci", default="18",
        type=int, help="Order of Fibonacci")
parser.add_argument('-s', action="store", dest="max_s", default="0.25",
        type=float, help="Max value of s")
parser.add_argument('-d', action="store", dest="max_points", default="256",
        type=int, help="Maximum number of data points")
parser.add_argument('-e', action="store", dest="solvent_e_density", type=float,
        default="0.3340", help="Solvent electron density")
parser.add_argument('-E', action="store", dest="exp_datafile", 
        required = True, help="Experimental scattering file")

result = parser.parse_args()

filecheck(result.pdbfile)
filecheck(result.exp_datafile)
filecheck(result.crysol)

pdb_prefix = os.path.splitext(os.path.basename(result.pdbfile))[0]
exp_prefix = os.path.splitext(os.path.basename(result.exp_datafile))[0]
out_dir = '{0}.{1}.CRYSOL'.format(pdb_prefix, exp_prefix)

assure_path_exists(out_dir)

child = pexpect.spawn('%s' % result.crysol)
child.logfile = open('%s' % 'crysolfit.log', 'w')
child.expect('Enter your option')
child.sendline('0')
child.expect('Brookhaven file name')
child.sendline('%s' % result.pdbfile)
child.expect('Maximum order of  harmonics')
child.sendline('%s' % result.order_harmonics)
child.expect('Order of Fibonacci grid')
child.sendline('%s' % result.order_Fibonacci)
child.expect('Maximum s value')
child.sendline('%s' % result.max_s)
child.expect('Number of points')
child.sendline('%s' % result.max_points)
child.expect('Account for explicit hydrogens?')
child.sendline('N')
child.expect('Fit the experimental curve')
child.sendline('Y')
child.expect('Enter data file')
child.sendline('%s' % result.exp_datafile)
child.expect('Subtract constant')
child.sendline('N')
child.expect('Angular units in the input file')
child.sendline('1')
child.expect('Electron density of the solvent')
child.sendline('%s' % result.solvent_e_density)
child.expect('Plot the fit')
child.sendline('N')
child.expect('Another set of parameters')
child.sendline('N')

for n in glob.glob('%s*' % pdb_prefix):
    if not n == '%s.pdb' % pdb_prefix:
        if os.path.isfile(n):
            os.rename('%s' % n, '{0}/{1}'.format(out_dir, n))

exit
