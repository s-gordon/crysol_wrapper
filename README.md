# README #

Accepts two required required options: -p (pdb structure file) and -E
(experimental scattering data). Several other options can be configured as
indicated by:
```sh
./crysol_wrapper -h
```

### What is this repository for? ###

* Quick summary
* Version 0.1b

### How do I install it ###

Easiest way to download is by using git:
```sh
git clone https://bitbucket.org/s-gordon/crysol_wrapper.git
```
Assuming you've got python 2.7+ installed along with pexpect (see dependencies below), you're good to go.

### How do I get set up? ###

* Dependencies
  - pexpect: `pip install pexpect`
* How to run tests
  - `./crysol_wrapper.py -h`

### Contribution guidelines ###

### Who do I talk to? ###

* Shane Gordon